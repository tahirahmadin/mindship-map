import { Button, Typography } from "@material-ui/core";
import React, { useCallback } from "react";
import { withRouter } from "react-router";
import app from "./base";
import {Link} from 'react-router-dom';

const SignUp = ({ history }) => {
  const handleSignUp = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password, password2 } = event.target.elements;
      if (password.value === password2.value) {
        try {
          await app.auth().createUserWithEmailAndPassword(email.value, password.value);
          history.push("/");
        } catch (error) {
          alert(error);
        }
      }
    },
    [history],
  );

  return (
    <div className="d-flex flex-row justify-content-center" >
      <div style={{ width: 500, height: 500, marginTop: 50,backgroundColor:'#f9f9f9',borderRadius:7,padding:20 }}>
        <h1 className="text-center">SignUp here</h1>
        <form onSubmit={handleSignUp} style={{ marginTop: 30 }}>
          <label className="labels">Email </label>
          <input name="email" type="email" placeholder="Email" className="textFieldDesign" />

          <label className="labels">Password </label>
          <input
            name="password"
            type="password"
            placeholder="Password"
            className="textFieldDesign"
          />
          <label className="labels">Confirm Password </label>
          <input
            name="password2"
            type="password"
            placeholder="Confirm Password"
            className="textFieldDesign"
          />

          <div className="text-center mt-3">
            <Button
              type="submit"
              variant="contained"
              style={{ backgroundColor: "#301934", color: "white", outline: "none" }}>
              SignUp{" "}
            </Button>
          </div>
        </form>
        <div className='text-center mt-3' style={{textDecoration:'none'}}>
        
        <Typography align='center' style={{textDecoration:'none'}}  component={Link} to={'/login'}>Already have an account? Login Here</Typography></div>
      </div>{" "}
    </div>
  );
};

export default withRouter(SignUp);
