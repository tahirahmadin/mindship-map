import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import app from "../base";
import { Button } from "@material-ui/core";



const drawerWidth = 220;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  
  },
  appBar: {
    backgroundColor:'#ffffff',
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  
  menuButton: {
    marginRight: theme.spacing(2),
    color:'black'
  },
  
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

const Appbar=() =>{
  const classes = useStyles();
 

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        style={{backgroundColor:'white'}}
      >
        <Toolbar style={{width:'100%', display:'flex', flexDirection:'row', justifyContent:'space-between'}}>
        
          
          <Typography variant="h6" align='center'noWrap style={{color:'black'}}>
            Mindship Map
          </Typography>
          <div className="float-right">
          <Button  style={{backgroundColor:'white',color:'black',outline:'none'}} onClick={() => app.auth().signOut()}>Sign out</Button>
        </div>
        </Toolbar>

      </AppBar>
    
     
   
        <div className={classes.drawerHeader} />
        
       
      
    </div>
  );
}





export default Appbar;
