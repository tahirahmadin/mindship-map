import {
  Button,
  Card,
  CardContent,
  CardMedia,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Fab,
  Typography,
} from "@material-ui/core";
import React, { Component } from "react";
import AppBar from "./component/AppBar";
import app from "./base";

import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import { LocationOn } from "@material-ui/icons";

const MyMapComponent = withScriptjs(
  withGoogleMap((props) => (
    <GoogleMap defaultZoom={4} defaultCenter={{ lat: 12.9716, lng: 77.5946 }} onClick={props.onClick}>
      {props.MarkerItems.map((item) => (
        <Marker key={item.latitude}position={{ lat: parseFloat(item.latitude), lng: parseFloat(item.longitude) }} />
      ))}
    </GoogleMap>
  )),
);

class Home extends Component {
  constructor() {
    super();
    this.state = {
      addPopup: false,
      places: [],
      myLat: "",
      myLong: "",
      fLat: null,
      fLong: null,
      fplace:null,
      markerPair: [],
      onSelectedLocMarkers:[],
     selectedLat:null,
     selectedLong:null,
      distance: null,
      loading: true,
    };
  }
  componentDidMount = () => {
this.fetchPlacesData();
    this.getCurrentLocation();
  };

fetchPlacesData=()=>{
  var allPlaces = [];
  //firestore fetch
  const db = app.firestore();
  db.collection("places")
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        allPlaces.push(doc.data());
        this.setState({ places: allPlaces, loading: false });
      });
    });
}
  getCurrentLocation = async () => {
    //getting current location of the user
   await navigator.geolocation.getCurrentPosition(
      (position) =>
        this.setState({
          myLat: position.coords.latitude,
          myLong: position.coords.longitude,
        }),
      (err) => console.log(err),
    );
    console.log(this.state.myLat);
  };




  handleAddLocation = async (event) => {
    event.preventDefault();
    const { place, address, } = event.target.elements;
//add a new place into database
    try {
      const db = app.firestore();
      db.settings({
        timestampsInSnapshots: true,
      });
       db.collection("places").add({
        place: place.value,
        address: address.value,
        latitude: this.state.selectedLat,
        longitude: this.state.selectedLong,
      });

      this.setState({ addPopup: false });
      this.props.history.push("/");
    } catch (error) {
      alert(error);
    }
    this.fetchPlacesData();
  };

  calculateDistance = (lat1, long1, lat2, long2) => {
   //Calculation of distance between two points.
    var R = 6371; 
    var dLat = this.toRad(lat2 - lat1);
    var dLon = this.toRad(long2 - long1);
    lat1 = this.toRad(lat1);
    lat2 = this.toRad(lat2);

    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
  };


  toRad(Value) {
    return (Value * Math.PI) / 180;
  }


  onClick=(event) =>{
    var lat = event.latLng.lat();
    var lng = event.latLng.lng();
   
 
    var markerPairs = [
      { latitude: lat, longitude: lng},
      
    ];
    this.setState({onSelectedLocMarkers:markerPairs,selectedLat:lat,selectedLong:lng});
  }




  onSelectPlace = (destLat, destLong,place) => {
    //On Place click setting states and updating marker and calcualtion of distance
    var markerPairs = [
      { latitude: this.state.myLat, longitude: this.state.myLong},
      { latitude: destLat, longitude: destLong },
    ];
    var distance = this.calculateDistance(this.state.myLat, this.state.myLong, destLat, destLong);
   
    this.setState({ markerPair: markerPairs, fLat: destLat, fLong: destLong,distance: Math.round(distance),fplace:place });
  };
  render() {
    let styles = {
      media: {
        height: 0,
        paddingTop: "56.25%", // 16:9
      },
    };
    return !this.state.loading ? (
      <div>
        <AppBar />
        <div className="float-right">
          <Fab style={{outline:'none',marginRight:10}} onClick={this.getCurrentLocation}>
            <LocationOn />
          </Fab>
          <Button
            variant="contained"
            style={{ backgroundColor: "purple", color: "white", outline: "none" }}
            onClick={() => this.setState({ addPopup: true })}>
            Add Location
          </Button>
        </div>
        <h4 className="text-center mb-5">Favourite Location Tracker</h4>

        <div className="row">
          <div className="col-md-6">
            <div className="row">
              {this.state.places.length !== 0
                ? this.state.places.map((row) => (
                    <div className="col-md-4" key={row.id}>
                      <Card
                    
                        onClick={() => this.onSelectPlace(row.latitude, row.longitude,row.place)}
                        className="d-flex flex-column justify-content-center mb-2"
                        style={{ height: 240, width: 200 }}>
                        <CardMedia
                          style={styles.media}
                          image="https://images.unsplash.com/photo-1522384731535-d1ccd4345321?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                          title="Dubai"
                        />
                        <CardContent>
                          <Typography
                            variant="h5"
                            color="textPrimary"
                            component="h1"
                            align="center">
                            {row.place}
                          </Typography>
                          <Typography variant="h6" color="textPrimary" component="p" align="center">
                            {row.address}
                          </Typography>
                          <Typography
                            variant="body2"
                            color="textPrimary"
                            component="p"
                            align="center">
                            {row.latitude}° - {row.longitude}°
                          </Typography>
                        </CardContent>
                      </Card>
                    </div>
                  ))
                : "No data found."}
            </div>
          </div>

          <div className="col-md-6" style={{ height: "80vh" }}>
            <MyMapComponent
              isMarkerShown
              MarkerItems={this.state.fLat === null ? this.state.places : this.state.markerPair}
              googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `95%` }} />}
              containerElement={<div style={{ height: `500px` }} />}
              mapElement={<div style={{ height: `95%` }} />}
            />
            <div className="mt-3">
              {this.state.distance !== null && (
                <Typography variant="h6" component="p">
                  Distance between you and your dream travel destination <strong style={{color:'red'}}>{this.state.fplace} </strong>is just{" "}
                  <strong style={{color:'green',fontSize:24}}>{this.state.distance}</strong> Kms.
                </Typography>
              )}
            </div>
          </div>
        </div>
        <Dialog open={this.state.addPopup}>
          <form onSubmit={this.handleAddLocation} style={{ marginTop: 30 }}>
            <DialogTitle style={{ height: 40 }}>Add new location</DialogTitle>
            <DialogContent>
              <DialogContentText>Kindly fill up the form to add new location </DialogContentText>
              <MyMapComponent
              isMarkerShown
              onClick={this.onClick}
              MarkerItems={this.state.onSelectedLocMarkers}
              googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `95%` }} />}
              containerElement={<div style={{ height: `500px` }} />}
              mapElement={<div style={{ height: `95%` }} />}
            />
              <label className="labels">Name </label>
              <input
                name="place"
                type="text"
                placeholder="Name of the place"
                className="textFieldDesign"
              />

              <label className="labels">Address </label>
              <input
                name="address"
                type="text"
                placeholder="Address of the place"
                className="textFieldDesign"
              />
              <div className="row">
                <div className="col-md-6">
                  <label className="labels">Latitude </label>
                  <input
                    name="latitude"
                    type="text"
                    value={this.state.selectedLat}
                    readOnly
                    placeholder="Address of the place"
                    className="textFieldDesign"
                  />
                </div>
                <div className="col-md-6">
                  <label className="labels">Longitude </label>
                  <input
                    name="longitude"
                    type="text"
                    value={this.state.selectedLong}
                    readOnly
                    placeholder="Address of the place"
                    className="textFieldDesign"
                  />
                </div>
              </div>
            </DialogContent>
            <DialogActions>
              <Button
                style={{ color: "#01579b", outline: "none" }}
                onClick={() => this.setState({ addPopup: false })}>
                Cancel
              </Button>
              <Button
                style={{ backgroundColor: "#301934", color: "white", outline: "none" }}
                type="submit">
                Add location
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    ) : (
     <div className='text-center mt-5'>
       <img src='https://miro.medium.com/max/978/0*mv8MNRLDNNnt5f72.gif' alt='loader'/>
     </div>
    );
  }
}
export default Home;
