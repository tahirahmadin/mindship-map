import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import app from "./base.js";
import { AuthContext } from "./Auth.js";
import { Button, Typography } from "@material-ui/core";
import {Link} from 'react-router-dom';
const Login = ({ history }) => {
  const handleLogin = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await app.auth().signInWithEmailAndPassword(email.value, password.value);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history],
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <div className="d-flex flex-row justify-content-center" >
      <div style={{ width: 600, height: 500, marginTop: 50,backgroundColor:'#f9f9f9',borderRadius:7,padding:20 }}>
        <h1 className="text-center">Log in</h1>
        <form onSubmit={handleLogin} style={{ marginTop: 30 }}>
          <label className="labels">Email </label>
          <input name="email" type="email" placeholder="Email" className="textFieldDesign" />

          <label className="labels">Password </label>
          <input
            name="password"
            type="password"
            placeholder="Password"
            className="textFieldDesign"
          />

          <div className="text-center mt-3">
            <Button
              type="submit"
              variant="contained"
              style={{ backgroundColor: "#301934", color: "white", outline: "none" }}>
              Login{" "}
            </Button>
          </div>
        </form>
      <div className='text-center mt-3' style={{textDecoration:'none'}}>
        
        <Typography align='center' style={{textDecoration:'none'}}  component={Link} to={'/signup'}>Don't have an account? SignUp Here</Typography></div>{" "}
        </div></div>
  );
};

export default withRouter(Login);
