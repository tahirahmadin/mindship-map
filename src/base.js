import * as firebase from "firebase/app";
import "firebase/auth";
import 'firebase/firestore';
require('dotenv').config();
const app = firebase.initializeApp({
  apiKey: process.env.REACT_APP_FIREBASE_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID,
  appId: "1:30821986011:web:03b041a5d2d3d73c8326fe",
  measurementId: "G-JGVG8F2Z3X"
});

export default app;
